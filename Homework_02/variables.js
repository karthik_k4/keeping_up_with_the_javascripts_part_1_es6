/*
HOISTING:

Hoisting is the process in JavaScript in which all the variable declarations 
are pushed to the top of the function or script at run time. This allows us 
to use the variables even before its declared. Some other features of hoisting:

1) During hoising, only the declaration is moved to the top of the function. But, 
the initialization is not moved to the top. For e.g., consider the statement: 
var value = 20; 
Here, var value is moved to the top of the scope. But, value = 20 retained in 
the same line where it was written.
2) Hoisting is applied only for variables declared as var. Any let and const 
items are not hoisted.
3) If 'strict mode' is turned on, hoisting is not applied.
4) Its always recommended to declare all the variables at the beginning rather 
than scattering the declarations all over the place.
5) Hoising also consumes time when the script is initially loaded. Although 
this is neglible, it still consumes time. So, more the number of variables 
declared, the longer hoisitng takes.
*/

/*
VARIABLES AND CONSTANTS:

Every programming language needs to store values using which operations 
are to be performed in the code. These are ususally referred to as variables 
in most of the languages. JavaScript supports 2 such aspects: Variables 
and Constants. 
Variables are ones whose values could change during the course of execution of the program. 
Constants are ones whose values remain the same throughout the liftime of the program.

Every varialbe/constant has a 'scope' which determines the life-time of the 
variable itself (i.e., how long does the variable reside in the memory and 
is available for the program to access/modify its value). How a variable 
is declared determines the scope of the variable in JavaScript. 

There are 3 ways of declaring Varialbes and Constants: var, let, const.

var:

var is the traditional form of declaring variables. 
When a variable is declared using var:
1) its value can be modified at a later point of time.
2) its scope is not limited to the block in which it is declared. The 
variable is available even outside the scope in which it is declared. 

Since var is available outside the block, it causes confusion while 
development and developer could potentially access variables that should 
have already been destroyed. Its best to avoid the usage of var as much as possible.


let:

let is one of the newer ways of declaring variables. 
1) Its value can be modified later on in the program.
2) Its scope is limited to the block in which it is declared. It is not 
available for usage outside the block in which its declared. Its best 
to use let as much as possible.

const:

const is similar to let. The only difference is that its values cannot 
be modified once an initial value is assigned.
1) Its value cannot be modified once the value is assigned.
2) Its scope is limited to the block in which its declared.

Note: In case of objects and arrays that are declared using const, the contents 
of the array/object can be modified. It is just that the constant cannot be 
assigned another object/array.

Note 2: Traditionally JavaScript allows declaring variables without using any 
of var, let, const. This makes JavaScript declare the variable in Global scope. 
Using variables should in this fashion should be avoided. We should only use 
let or const to declare variables and constants. 

*/

//Function to demo var
function varDemo() {
    var shouldSayHi = true;
    var firstName = "John";

    if (shouldSayHi === true) {
        //lastName is declared within this block
        var lastName = "Smith";

        console.log("Hello " + firstName + " " + lastName); //prints both first and last name
    }

    lastName = "Miller"; //Value of a 'var' variable can be modified.

    //Prints both first and last name even though lastName is out of scope here.
    console.log("Hello " + firstName + " " + lastName);
}

//Function to demo let
function letDemo() {
    let shouldSayHi = true;
    let firstName = "John";

    if (shouldSayHi === true) {
        //lastName is declared within this block but this time using let
        let lastName = "Smith";
        console.log("Hello " + firstName + " " + lastName); //prints both first and last name
    }

    firstName = "Steve"; //Value of a 'let' variable can be modified.

    //JavaScript throws an undefined error since lastName is not part of current scope.
    console.log("Hello " + firstName + " " + lastName);
}

//Function to demo const
function constDemo() {
    const shouldSayHi = true;
    const firstName = "John";

    if (shouldSayHi === true) {
        //lastName is declared within this block using const
        const lastName = "Smith";
        console.log("Hello " + firstName + " " + lastName); //prints both first and last name
    }

    firstName = "Steve"; //Value of a 'const' cannot be modified. Lint throws an error and the code does not execute

    //JavaScript throws an undefined error since lastName is not part of current scope.
    console.log("Hello " + firstName + " " + lastName);
}

//Function to demo Array/object declared as const
function constArrayDemo() {
    const names = ["David", "Steve", "John"];
    //Its possible to add new items to the array even though names is declared using const.
    names.push("Amber");
    console.log(names); //prints ["David", "Steve", "John", "Amber"]

    //But, assigning a new array to names is not possible since its a const. Same is with an object
    names = ["Jerry", "Nicole", "Jimmy"];

    console.log(names);
}

function pageDidLoad() {
  addHeadings();
  setInnerDivColor();
  listHexCodes(); 
  displayAllDivIDs();
  setDocumentTitle()
}

//Function that adds H1 and H2 tags to the beginning of the body.
function addHeadings() {
  const h1 = document.createElement("h1");
  h1.innerHTML = "Color Pallette";
  h1.id = "h1Heading"
  h1.style.color = "#A569BD";
   
  const h2 = document.createElement("h2");
  h2.innerHTML = "List of 10 colors and their HEX values";
  h2.style.color = "#2E86C1";

  const outerDiv = document.getElementById("rectangleWrapper");
  document.body.insertBefore(h1, outerDiv);
  document.body.insertBefore(h2, outerDiv);
}

//Function to get the text in the H1 tag and set it as the page title
function setDocumentTitle() {
  const h1 = document.getElementById("h1Heading");
  document.title = h1.innerText;
}

//Function to set the color for each of the Divs in the HTML page
function setInnerDivColor() {
  const colors = ["#F5B041", "#F7DC6F", "#58D68D", "#A569BD", "#E74C3C", "#2E86C1", "#FFA07A", "#641E16", "#F5B7B1", "#008080"];
  
  const allDivs = document.getElementsByClassName("customDiv");
  
  var index;  
  for(index in colors) {
    allDivs[index].style.backgroundColor = colors[index];
  }
}

//Function to get the background color from the Div and set it to the Span element.
function listHexCodes() {
  const allDivs = document.getElementsByClassName("customDiv");
  const allSpans = document.getElementsByClassName("customSpan");
  
  for(var index = 0; index < allDivs.length; index++) {
    let hex = getHexfromRGB(allDivs[index].style.backgroundColor);
    allSpans[index].textContent = hex;
  }
}

//Function which gets the identifier of each of the Divs and display it in the console.
function displayAllDivIDs() {
  const allDivs = document.getElementsByClassName("customDiv");
  
  console.log("Here are the rectangle IDs:");
  var item;
  for(item of allDivs) {
    console.log(item.id);
  }
}

//Function takes in RGB color in string format and converts it into Hexadecimal.
function getHexfromRGB(color) {
  const rgb = color.split("(")[1].split(")")[0].split(", ");
  
  let r = ("0" + parseInt(rgb[0], 10).toString(16)).slice(-2);
  let g = ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2);
  let b = ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2);
  
  return ("#"+r+g+b).toUpperCase();
}
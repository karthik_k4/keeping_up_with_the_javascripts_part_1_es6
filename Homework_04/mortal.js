//Lets create a constructor that in turn returns an object.
//The constructor takes in a boolean parameter (mortality) that sets the isMortal parameter of the object. The second parameter takes in the list of names to be checked.

function Sect(mortality, ...list) {
  this.mortality = mortality;
  //Convert the name to lowercase so that its easy to compare.
  this.names = list.map((element) => element.toLowerCase());
  
  //Function takes in a name and returns if its present in the list or not.
  this.isNamePresent = (name) => this.names.includes(name.toLowerCase());
}

const isMortal = (name) => {
  //Function takes in a name and returns if its present in the list or not.
  if(typeof(name) !== "string") {
    return false;
  }
  
  //Create an object of Sect named 'men' and we pass 'mortality' parameter as 'true'
  //Also pass in the list of names (Which is a dynamic list).
  const mortals = new Sect(true, "Socrates", "Plato", "Leonaides", "Alexander", "Gelileo");
  
  return mortals.isNamePresent(name);
}

const name = "Socrates" ;

if(isMortal(name)) {
  console.log(name + " is a Man");
} else {
  console.log(name + " is not a Man");
}

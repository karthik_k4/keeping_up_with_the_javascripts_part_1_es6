//Create an object of Cake that can store the flavor, supported flavors and functions to check the flavors
function Cake(flavors, cakeFlavor) {
    this.flavor = cakeFlavor;
    this.supportedFlavors = availableFlavors.map((element) => element.toLowerCase());

    //function checks if the flavor is supported or not
    this.isFlavorRight = function() {
        if (this.supportedFlavors.includes(this.flavor.toLowerCase())) {
            return true;
        }

        return false;
    };

    //function returns the flavor name if its valid else returns an error
    this.checkCake = () => {
        if (this.isFlavorRight() === true) {
            return this.flavor;
        } else {
            return "Unsupported Flavor";
        }
    };
}

availableFlavors = ["Chocolate", "Vanilla"];
cakeFlavor = "Chocolate"; 
let cake = new Cake(["Chocolate", "Vanilla"], cakeFlavor);
console.log(cake.checkCake());



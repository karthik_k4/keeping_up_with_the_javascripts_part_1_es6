function checkPrime(num) {
    
    if(num < 2) {
        return false;
    }
    
    let isPrime = true;
    
    for(let j = 2; j <= num / 2; j++) {
        if(num % j === 0) {
            isPrime = false;
            break;
        }
    }

    return isPrime;
}

function fizzBuzzPrime(minLimit, maxLimit, fizz, buzz) {
    
    for(let num = minLimit; num <= maxLimit; num++) {
        if(num % fizz === 0 && num % buzz === 0) {
            console.log("FizzBuzz");
        } else if(num % fizz === 0) {
            console.log("Fizz");
        } else if(num % buzz === 0) {
            console.log("Buzz");
        } else if(checkPrime(num) === true) {
            console.log("Prime");
        } else {
            console.log(num);
        }
    }
}


//////////////////////////////

const fizz = 3;
const buzz = 5;
const minLimit = 1;
const maxLimit = 100;

fizzBuzzPrime(minLimit, maxLimit, fizz, buzz);


//Function to check validity of the parameters.

function checkParamsValidity(time1, unit1, time2, unit2) {
    //Return false if the type of parameters is wrong.
    if((checkTypeof(time1, "number") && 
        checkTypeof(unit1, "string") && 
        checkTypeof(time2, "number") && 
        checkTypeof(unit2, "string")) === false) {
        
        console.log("Parametes are of wrong type");
        return false;
    }
    
    //Check if the units are proper.
    if((checkUnits(unit1) && 
        checkUnits(unit2)) === false) {
        
        console.log("Labels are invalid");
        return false;
    }
    
    //Check if values are positive integers 
    
    if (time1 < 0 ||
        time2 < 0 ||
       Number.isInteger(time1) === false ||
       Number.isInteger(time2) === false) {
        
        console.log("Values are not positive integer");
        return false;
    }
    
    //Check plurality
    
    if((checkPlurality(time1, unit1) && 
        checkPlurality(time2, unit2)) === false) {
        
        console.log("Value and labels mismatch")
        return false;
    }
    
    return true;
}

function checkTypeof(value, type) {
  return typeof(value) === type;
}

//const value1 = 10;
//const label1 = "Hours";
//const value2 = 20;
//const label2 = "Hours";
//
//console.log(checkParamsValidity(value1, label1, value2, label2));

function checkUnits(unit) {
    switch(unit.toLowerCase()) {
        case "seconds": 
        case "minutes": 
        case "hours": 
        case "days": 
        case "second": 
        case "minute": 
        case "hour": 
        case "day":
            return true;
            break;
            
        default:
            return false;
            break;
    }
    
    return false;
}

function checkPlurality(value, unit) {
    switch(unit.toLowerCase()) {
        case "seconds": 
        case "minutes": 
        case "hours": 
        case "days": 
            if(value !== 1) {
                return true;
            }
            break;
            
        case "second": 
        case "minute": 
        case "hour": 
        case "day":
            if(value === 1) {
                return true;
            }
            break;
            
        default:
            break;
    }
    
    return false;
}

//Function to convert Days or Hours or Minutes to seconds.

function convertToSeconds(time, unit) {
    switch(unit.toLowerCase()) {
        case "seconds": 
        case "second": 
            return time;
            break;
            
        case "minutes": 
        case "minute":
            return time * 60;
            break;
            
        case "hours": 
        case "hour": 
            return time * 3600; //time * 60 * 60
            break;
            
        case "days": 
        case "day":
            return time * 86400; //time * 60 * 60 * 24
            break;
            
        default:
            return 0;
            break;
    }
    
    return 0;
}


function convertSecondsToUnits(seconds, unit) {
    
    switch(unit.toLowerCase()) {
            
        case "seconds": 
        case "second": 
            return seconds;
            break;
            
        case "minutes": 
        case "minute":
            return Math.floor(seconds / 60);
            break;
            
        case "hours": 
        case "hour": 
            return Math.floor(seconds / 3600);
            break;
            
        case "days": 
        case "day":
            return Math.floor(seconds / 86400);
            break;
            
        default:
            break;
    }
    
    return 0;
}

//Function to recursively convert seconds to Days, Hours, Minutes and Seconds format.
//The result is stored in a Time object.

function convertSecondsToAllUnits(seconds, unit, time) {
    
    switch(unit.toLowerCase()) {
            
        case "seconds": 
        case "second": 
            time.seconds = seconds;
            break;
            
        case "minutes": 
        case "minute":
            time.minutes = Math.floor(seconds / 60);
            seconds = seconds % 60;
            convertSecondsToAllUnits(seconds, "seconds", time);
            break;
            
        case "hours": 
        case "hour": 
            time.hours = Math.floor(seconds / 3600);
            seconds = seconds % 3600;
            convertSecondsToAllUnits(seconds, "minutes", time);
            break;
            
        case "days": 
        case "day":
            time.days = Math.floor(seconds / 86400);
            seconds = seconds % 86400;
            convertSecondsToAllUnits(seconds, "hours", time);
            break;
            
        default:
            break;
    }
}


function assignUnit(time, unit) {
    
    if(time === 1) {
        switch(unit.toLowerCase()) {
            case "seconds": 
            case "second": 
                return "second";
                break;

            case "minutes": 
            case "minute":
                return "minute";
                break;

            case "hours": 
            case "hour": 
                return "hour";
                break;

            case "days": 
            case "day":
                return "day";
                break;

            default:
                break;
        }
    } else {
        switch(unit.toLowerCase()) {
            case "seconds": 
            case "second": 
                return "seconds";
                break;

            case "minutes": 
            case "minute":
                return "minutes";
                break;

            case "hours": 
            case "hour": 
                return "hours";
                break;

            case "days": 
            case "day":
                return "days";
                break;

            default:
                break;
        }
    }
    
    return "";
}


function Time() {
    this.days = 0;
    this.hours = 0;
    this.minutes = 0;
    this.seconds = 0;
    
    this.checkLargestLabel = () => {
        if(this.seconds !== 0) {
            return "seconds";
        } else if(this.minutes !== 0) {
            return "minutes";
        } else if(this.hours !== 0) {
            return "hours";
        } else {
            return "days";
        }
        
        return "seconds";
    }
    
    this.reset = () => {
        this.days = 0;
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
    }
    
    this.display = () => {
        console.log("Days: " + this.days);
        console.log("Hours: " + this.hours);
        console.log("Minutes: " + this.minutes);
        console.log("Seconds: " + this.seconds);
    }
}

function timeAdder(value1, label1, value2, label2) {
    if(checkParamsValidity(value1, label1, value2, label2) === false){
      console.log("Errors in input");
      return false;
    }
  
    const totalSeconds = convertToSeconds(value1, label1) + convertToSeconds(value2, label2);
    console.log(totalSeconds);
    
    //Extra credits
    let time = new Time();
    convertSecondsToAllUnits(totalSeconds, "days", time);
    
    const largestLabel = time.checkLargestLabel();
    const finalTime = convertSecondsToUnits(totalSeconds, largestLabel);
    const finalUnit = assignUnit(finalTime, largestLabel);

    return [finalTime, finalUnit];
}

////////////////////////////////////////////////

//---------Convert Days/Hours/Mins to seconds and vice-versa

//console.log(convertToSeconds(25, "Minutes"));

//console.log(convertSecondsToUnits(3600, "Minutes"));

//---------Convert seconds to Days/Hours/Mins/Seconds

const value1 = 60;
const label1 = "Minutes";
const value2 = 2;
const label2 = "Hours";

//var timeObject = new Time();
//const seconds = convertToSeconds(value1, label1) + convertToSeconds(value2, label2)
//convertSecondsToAllUnits(seconds, "days", timeObject);
//timeObject.display();
//console.log(timeObject.checkLargestLabel());

console.log(timeAdder(value1, label1, value2, label2));





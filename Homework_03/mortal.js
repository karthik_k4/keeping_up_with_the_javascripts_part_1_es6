//Method 1: This is more of a static approach.
//Lets have an object named 'men'.
//This contains 'isMortal' bool attribute. This is set to true.
//The names array contains  the list of all men.
//We check if the names object contain 'Socrates' and if so we print that Socrates is a man.

const men = {
    isMortal: true,
    names: ["Plato", "Alexander", "Leonaides", "Socrates", "Gelileo"],

    checkMortality: function(name) {
        if (this.names.includes(name) === true) {
            console.log(name + " is a man");
            return true;
        } else {
            console.log(name + " is not a man");
            return false;
        }
    }
};

if (men.isMortal === true && men.checkMortality("Socrates") === true) {
    console.log("Socrates is Mortal");
} else {
    console.log("Socrates is not Mortal");
}

//Alternatively, we can have a more dynamic approach.
//Lets create a constructor that in turn returns an object.
//The constructor takes in a boolean parameter (mortality) that sets the isMortal parameter of the object.
//The 'addName' method adds one name of a person to the object.
//The names are stored in the array 'names'.
//We check if the names object contain 'Socrates' and if so we print that Socrates is a man.

function Sect(mortality) {
    this.isMortal = mortality;
    this.names = [];

    this.checkMortality = function(name) {
        if (this.names.includes(name)) {
            console.log(name + " is a man");
            return true;
        } else {
            console.log(name + " is not a man");
            return false;
        }
    };

    this.addNewName = function(name) {
        this.names.push(name);
        console.log(this.names);
    };
}

//Create an object of Sect named 'men' and we pass 'mortality' parameter as 'true'
const men = new Sect(true);
//Add a list of names to men.
men.addNewName("Socrates");
men.addNewName("Plato");
men.addNewName("Leonaides");
men.addNewName("Alexander");
men.addNewName("Gelileo");
//Check if 'Socrates' is mortal:
const name = "Socrates";
console.log("Men contains the names: " + men.names);

//If the isMortal field is true and if the names array of men contains Socrates, Socrates is mortal.
if (men.isMortal === true && men.checkMortality(name) === true) {
    console.log(name + " is Mortal");
} else {
    console.log(name + " is not Mortal");
}

//Extra Credits

//Create an object of Cake that can store the flavor, supported flavors and functions to check the flafors
function Cake(cakeFlavor) {
    this.flavor = cakeFlavor;
    this.supportedFlavors = ["chocolate", "vanilla"];

    //function returns true if the flavor is Chocolate
    this.isChocolate = function() {
        if (this.flavor.toLowerCase() == "chocolate") {
            return true;
        }

        return false;
    };

    //function checks if the flavor is supported or not
    this.isFlavorRight = function() {
        if (this.supportedFlavors.includes(this.flavor.toLowerCase())) {
            return true;
        }

        return false;
    };

    //function checks if the flavor is valid and if its Chocolate or Vanilla
    this.checkCake = function() {
        if (this.isFlavorRight() === true) {
            if (this.isChocolate() !== true) {
                console.log("Cake is Vanilla");
            } else {
                console.log("Cake is Chocolate");
            }
        } else {
            console.log("Cake is not the right flavor");
        }
    };
}

let chocolateCake = new Cake("CHOcolate");
chocolateCake.checkCake();

let vanillaCake = new Cake("Vanilla");
vanillaCake.checkCake();

let blueberryCake = new Cake("blueberry");
blueberryCake.checkCake();

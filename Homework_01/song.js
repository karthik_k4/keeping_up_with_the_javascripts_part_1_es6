//Title of the song.
var title = "Krishna Nee Begane Baro";

//The main singer of the song
var artist = "Hariharan";

//The original composer/writer
var composer = "Kanakadasa";

//Duration of the song in seconds
var length = 345;

/*
    Arists who provided music to the song.
    The keys mention the instrument name.
    The values mention the name of the artist for the instrument mentioned in the key.
*/
var backgroundMusic = {
    voilin: "Kannakudi Vaidyanathan",
    veena: "Sheshanna",
    ghatam: "Govindraju",
    tabla: "Zakir Hussain",
    flute: "Hariprasad Chaurasia"
};

//Year in which the song was released.
var year = 1998;

//Exact date of the song being released.
var creationDate = "1998-04-03 10:20:30";

//Genre
var genre = "Carnaic fusion";

//Album to which the song belongs to.
var album = "Bhaja Govindam";

//Country in which the song was created.
var countryOfOrigin = "India";

//Unique id for the song to assist in uniquely identify the song among a group of songs.
var id = "1234567890";

//The band that created the song.
var band = "Colonial Cousins";

//Whether the band is active as of today.
var isBandActive = true;

//Members belonging to the band. Returned as an array of artist names.
var bandMembers = ["Hariharan", "Lesle Lewis"];

//Studio in which the song was recorded.
var recordingStudio = "Madras music studios";

//Array of Streamable URIs to listen to the music online.
var uris = [
    "http://music.domain.com/release/1",
    "http://music.domain.com/release/2"
];

//Whether the song is copyrighted.
var isCopyrighted = true;

//Has the copyright expired.
var copyrightExpired = false;

//Copyright statement.
var copyright = "Copyright 1992-2019, Colonial cousians music";

/*
    The official images to be displayed in the apps or websites.
    The images are provided in 3 different sizes: Large, Medium, Small.
    The apps can use any of the images based on the necessity.
    The object has 3 keys: large, medium, small (one each for each of the image sizes).
*/
var images = {
    large: {
        height: 640,
        width: 640,
        url: "https://image.domain.com/path/to/image/640/640x640.jpeg"
    },
    medium: {
        height: 300,
        width: 300,
        url: "https://image.domain.com/path/to/image/300/300x300.jpeg"
    },
    small: {
        height: 60,
        width: 60,
        url: "https://image.domain.com/path/to/image/60/60x60.jpeg"
    }
};

//Console Logs for all the attributes of the song.

console.log(title);
console.log(artist);
console.log(composer);
console.log(length);
console.log(backgroundMusic);
console.log(year);
console.log(creationDate);
console.log(genre);
console.log(album);
console.log(countryOfOrigin);
console.log(id);
console.log(band);
console.log(isBandActive);
console.log(bandMembers);
console.log(recordingStudio);
console.log(isCopyrighted);
console.log(copyrightExpired);
console.log(copyright);
console.log(images);
